# Bill Of Materials

Please update the file with every new compnent

| Compenent                 |Model      |Qauntity|Price[DKK]|Purchase link                                                                                                                                       |Datasheet |
| ------------              | ------------  |--| ------- | ------------ | ------------ |
|ADC                        |MCP3008-I/P*   |1 |23.59 |https://www.digikey.dk/da/products/detail/microchip-technology/MCP3008-I-P/319422                                                                  |https://ww1.microchip.com/downloads/en/DeviceDoc/21295d.pdf
|Digital isolator           |ISO7310CDR     |4 |21    |https://www.digikey.dk/da/products/detail/texas-instruments/ISO7310CDR/4946350?s=N4IgTCBcDaIJIGUDyB2AzARgAwGEAiASiALoC%2BQA                        |https://www.ti.com/general/docs/suppproductinfo.tsp?distId=10&gotoUrl=https%3A%2F%2Fwww.ti.com%2Flit%2Fgpn%2Fiso7310c
|Rasberry Pi                |4B             |1 |295   |https://raspberrypi.dk/en/product/raspberry-pi-4-model-b-1-gb/                                                                                     |
|Female pin headers 2x20    |PPTC202LFBN-RC |1 |17.15 |https://www.digikey.dk/da/products/detail/sullins-connector-solutions/PPTC202LFBN-RC/807240?s=N4IgTCBcDaIAQAUEBUDCYAMYAyAxAQgHIC0ASqiALoC%2BQA     |
|Female pin header 1x4      |PPPC041LFBN-RC |1 |3.5   |https://www.digikey.dk/da/products/detail/sullins-connector-solutions/PPPC041LFBN-RC/810176                                                        |
|Screw terminals            |282836-2*      |4 |7.90  |https://www.digikey.dk/da/products/detail/te-connectivity-amp-connectors/282836-2/1826939?s=N4IgTCBcDaIMwDYCMBaA7AhaQF0C%2BQA
|Oled display               |ssd1306*       |1 |36    |https://www.ebay.com/itm/255303518853?hash=item3b71468a85:g:pqsAAOSwGoBb4LDb&amdata=enc%3AAQAHAAAA4E2JWZhhtz9ocW9y6M81kptkIlOLxCqbFHybLIxBU3tIXmo7y3cWOemVZCWCYlDQT4eCdPdvxu4QOT6RqhR%2FB1seioZzakHL4y8A2QoyVO1AuNlPjIZXLa47aXxDYWAJAUg4gSG0McWYTEnzOVn8G71guEJAMeQG3mOCYlIXdSknina2Q%2Fvyn4W6IiXXWs5hn0XyAWps32MOUqojTKMFI%2FLlu3JyXLtqFPTf4%2FxfTPSVDxmS7g2U8pSZciyWVQEL3LBM9rVksRLkRSprtU%2FTaENWYY%2FBOzpIcZH9rUPeGOPw%7Ctkp%3ABFBMoLvcnJFh                                                                                                                                              |
|Zener Diode                |BZT52C5V6-13   |5 |1.65  |https://www.digikey.dk/da/products/detail/diodes-incorporated/BZT52C5V6-13-F/1534399                                                               |
|Resistor                   |1206 1k        |20|0.63  |https://www.digikey.dk/da/products/detail/stackpole-electronics-inc/RNCP1206FTD1K00/2240337?s=N4IgTCBcDaIE4DsDGAHAjGADANgGYBcATNAa00yXwFoFCQBdAXyA
|Resistor                   |1206 10k*      |1 |0.75  |https://www.digikey.dk/da/products/detail/stackpole-electronics-inc/RNCP1206FTD10K0/2240370?s=N4IgTCBcDaIE4DsDGAHAjGADANgGYBcATNTAa0yXwFoFCQBdAXyA
|Capacitor 10uf             |1210*          |1 |1.43  |https://www.digikey.dk/da/products/detail/samsung-electro-mechanics/CL31A106KAHNNNE/3886733?s=N4IgTCBcDaIIxgOwDYC0cAMiCs7UDsATEAXQF8g              |
|Capacitor 4.7uf            |1210*          |1 |1.35  |https://www.digikey.dk/da/products/detail/samsung-electro-mechanics/CL31B475KAHNNNE/3886713?s=N4IgTCBcDaIIxgOwDYC0cAMBWL7UDsATEAXQF8g
|DC - DC isolated converter |177920511      |1 |42.11 |https://www.digikey.dk/da/products/detail/w%C3%BCrth-elektronik/177920511/6621997                                                                  |https://www.we-online.com/katalog/datasheet/1779205311.pdf|
|Switch                     |EVQ-PUA02K     |2 |4.71  |https://www.digikey.dk/da/products/detail/panasonic-electronic-components/EVQ-PUA02K/286334?s=N4IgTCBcDaIKIDUCKBaACgVQIIAYwGkQBdAXyA               |
|LED                        |150120GS75000* |1 |1.78  |https://www.digikey.dk/da/products/detail/w%C3%BCrth-elektronik/150120GS75000/4489936                                                              |
|Stand off                  |971110155      |4 |3.4   |https://www.digikey.dk/da/products/detail/w%C3%BCrth-elektronik/970110151/6174769                                                                  |https://www.we-online.com/katalog/datasheet/970xxxxx1_overview.pdf|
|Skruer                     |50M025045P012* |10|3.7   |https://www.digikey.dk/da/products/detail/essentra-components/50M025045P012/11639132?s=N4IgTCBcDaIKwAYC2CyICxwAoIIwQF0BfIA
|Voltage reference          |ADR5045        |2 |12.66 |https://www.digikey.dk/da/products/detail/analog-devices-inc/ADR5045BRTZ-REEL7/1534669                                                             |https://www.analog.com/media/en/technical-documentation/data-sheets/ADR5040_5041_5043_5044_5045.pdf
|Skrue M3 10m               |97791003111    |8 |2.08  |https://www.digikey.dk/da/products/detail/w%C3%BCrth-elektronik/97791003111/10056389                                                               |https://www.we-online.com/katalog/datasheet/97791003111.pdf|
|PCB fabrication            |               |  |26    |
|**TOTAL without pi and components in ELAB**|| |351.36|
*ELAB
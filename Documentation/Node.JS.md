# Official Documentation

## Socket.IO
https://socket.io

---

## OPCUA References
https://reference.opcfoundation.org/Core/Part4

---

## Node-OPCUA Client API References
https://node-opcua.github.io/api_doc/2.32.0/classes/node_opcua.opcuaclient.html

---
<br>

# Guides & Examples

## Node.JS Webapp 101
https://www.c-sharpcorner.com/article/building-web-application-using-node-js/

## Project Setup
A guide for setting up the webapp can be found  
in the [Node.JS folder](https://gitlab.com/industrial-applications/extended-regulator-interface/-/tree/main/Node.JS) of this repository

## Github Example Projects
https://github.com/node-opcua/node-opcua/tree/master/documentation
https://github.com/node-opcua/node-opcua-htmlpanel/
<br>
<br>
# TO-DO
Use chart.js to create chart:  
https://codepen.io/benni_de/pen/ajGood  
https://stackoverflow.com/questions/51686993/auto-remove-old-datapoints-in-chart-js/51687297#51687297  
??( https://www.chartjs.org/docs/latest/developers/updates.html )??
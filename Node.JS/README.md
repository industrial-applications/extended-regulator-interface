# Webapp Setup Guide
## Prerequisites
 - Node.JS
 - NPM
 - Cloned/Pulled this project  

## Instructions
1. Navigate to *extended-regulator-interface/Node.JS/Webapp* in a CLI
2. Install Dependencies with *npm install #*
3. Start the app with *npm start*  

### Note
Further documentation can be found in  
the [Documentation folder](https://gitlab.com/industrial-applications/extended-regulator-interface/-/blob/main/Documentation/Node.JS.md) of this project
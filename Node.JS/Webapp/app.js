//Network and Socket IO imports
interfaces = require('os').networkInterfaces();
const express = require("express");
const http = require("http");
const { Server } = require("socket.io");
const port = 80;

const { AttributeIds, OPCUAClient, TimestampsToReturn } = require("node-opcua");
var IP = "10.0.0.10";

const endpointUrl = "opc.tcp://" + IP + ":4840";
const nodeIdsToMonitor = ["ns=2;s=temperature", "ns=2;s=valve", "ns=2;s=flow", "ns=2;s=pressure"];

let client, session, subscription;
var data = {temperature:[], valve:[], flow:[], pressure:[]};

async function createOPCUAClient(io) {
  client = OPCUAClient.create({
    endpointMustExist: false,
  });
  client
  	.on("backoff", (retry, delay) => {
    	console.log("Retrying to connect to ", endpointUrl, " in ", delay/1000, "seconds. Attempt ", retry);
  	})
	.on("connection_lost", () => {
		console.log("LOST CONNECTION");
    	if (subscription) subscription.terminate();
  	})
	.on("connected", () => {
    	console.log("connected to ", endpointUrl);
  	})
	  .on("connection_reestablished", () => {
    	console.log("REconnected to ", endpointUrl);
		createSubscription(io);
  	});

  console.log("Connecting to ", endpointUrl);
  await client.connect(endpointUrl);

  session = await client.createSession();
  session
  	.on("session_closed", (StatusCode) => {
    	console.log("SESSION CLOSED status code: ", StatusCode);
  	})
	.on("session_restored", () => {
    	console.log("SESSION RESTORED");
  	});
  console.log("session created".yellow);
  await createSubscription(io);
}

async function createSubscription(io)
{
	subscription = await session.createSubscription2({
		requestedPublishingInterval: 100,
		requestedMaxKeepAliveCount: 30,
		requestedLifetimeCount: 100,
		maxNotificationsPerPublish: 10,
		publishingEnabled: true,
		priority: 0,
	});
	
	subscription
	.on("keepalive", function () {
		console.log("keepalive");
	})
	.on("terminated", function () {
		console.log("Subscription TERMINATED");
	})
	
	var itemsToMonitor = [];
	nodeIdsToMonitor.forEach(element => { 
		itemsToMonitor.push({nodeId: element, attributeId: AttributeIds.Value});
	});

	const parameters = {
		samplingInterval: 1000,
		discardOldest: true,
		queueSize: 100,
	};
	const monitoredItems = await subscription.monitorItems(
		itemsToMonitor,
		parameters,
		TimestampsToReturn.Source
		);

	monitoredItems.on("changed", (item, dataValue, index) => {
		_time = dataValue.sourceTimestamp.toLocaleString('en-UK').split(",")[1].trim();
		_value = dataValue.value.value;
		_nodeId = item.itemToMonitor.nodeId.value;
		console.log(_nodeId, _value, _time);
		var msg = {
			value: _value,
			time: _time,
		};
		data[_nodeId].push(msg);
		if (data[_nodeId].length >= 100){
			data[_nodeId].shift();
		}
		io.sockets.emit("message", msg, _nodeId);
	});
}

async function stopOPCUAClient() {
  if (subscription) await subscription.terminate();
  if (session) await session.close();
  if (client) await client.disconnect();
}

(async () => {
  try {
    // --------------------------------------------------------
    const app = express();
    app.use(express.static(__dirname + '/public'));
    app.get("/", function (req, res) {
      res.render("index.html");
    });
    const server = http.createServer(app);
    const io = new Server(server);
    io.sockets.on("connection", function (socket) {
		socket.emit('init_data', data);
	});

    server.listen(port, IP, () => {
      console.log("Listening on port " + port);
      console.log("http://" + IP + ":" + port);
    });

    // --------------------------------------------------------
    createOPCUAClient(io);

    // detect CTRL+C and close
    process.once("SIGINT", async () => {
      console.log("shutting down client");

      await stopOPCUAClient();
      console.log("Done");
      process.exit(0);
    });
  } catch (err) {
    console.log("Error" + err.message);
    console.log(err);
    process.exit(-1);
  }
})();

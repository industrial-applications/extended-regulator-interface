# Problem
An old heating system is used to teach regulation to students in Vejle. With the current setup, the student are measuringthe temperature manually through a voltage output on the system. There is a wish to upgrade the system so that the students no longer have to manually measure the temperature and potentially also extend the measurements to some of the other data that is available in the system, like flow and pressure.

The solution need to include two parts, data collection and data availability.

Collecting the data comes with the challenge that the temperature is given in voltage range of 0V –10V. This means the solution needs to account for the higher voltage range that is not directly supported by microcontrollers. As the system is old, it is also a good idea to make sure that potential voltage spikes does not destroy the controller used.

Data availability in the contextof this project means two things, GUI and OPC UA interface. The GUI should include the current values of the collected data in a way that is easily readable. The OPC UA interface should expose the collected data to OPC UA client.

NodeRed could be a good place to start, but it is not required to use it.

Resources

[opcfoundation.org/about/opc-technologies/opc-ua/](https://opcfoundation.org/about/opc-technologies/opc-ua/)  
[wikipedia.org/wiki/OPC_Unified_Architecture](https://en.wikipedia.org/wiki/OPC_Unified_Architecture)  
[youtube.com/watch?v=-tDGzwsBokY&ab_channel=TheOPCFoundation](https://www.youtube.com/watch?v=-tDGzwsBokY&ab_channel=TheOPCFoundation)  
[pypi.org/project/opcua](https://pypi.org/project/opcua/)  
[flows.nodered.org/node/node-red-contrib-opcua](https://flows.nodered.org/node/node-red-contrib-opcua)  
[realpython.com/python-gui-tkinter/](https://realpython.com/python-gui-tkinter/)  

# Python requirements
python3 -m venv venv  
source venv/bin/activate  
pip install -r requirements.txt

## AUTO startup
sudo nano /etc/rc.local

Add :

```bash
bash /home/pi/Documents/extended-regulator-interface/RaspberryCode/startup.sh & 
exit 0
```

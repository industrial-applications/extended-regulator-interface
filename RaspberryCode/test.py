from time import sleep
from datetime import datetime
from board import I2C
from PIL import Image, ImageDraw, ImageFont
from adafruit_ssd1306 import SSD1306_I2C
from opcua import Server, ua
from gpiozero import Button, MCP3008

# VARIABLES -----

button = Button(17)

# Display Parameters
WIDTH = 128
HEIGHT = 64
BORDER = 5

# Use for I2C.
oled = SSD1306_I2C(
    WIDTH, HEIGHT, I2C(), addr=0x3C)

# Create blank image for drawing.
# Make sure to create image with mode '1' for 1-bit color.
image = Image.new("1", (oled.width, oled.height))

# Get drawing object to draw on image.
draw = ImageDraw.Draw(image)

font_small = ImageFont.truetype(
    '/home/pi/Documents/extended-regulator-interface/RaspberryCode/PixelOperator.ttf', 16)
font_large = ImageFont.truetype(
    '/home/pi/Documents/extended-regulator-interface/RaspberryCode/PixelOperator.ttf', 36)

# Setting ADC channels and inputs
ADC_Temp = MCP3008(0)  # ADC Channel 0
ADC_Valve = MCP3008(1)  # ADC Channel 1
ADC_Pres = MCP3008(2)  # ADC Channel 2
ADC_Flow = MCP3008(3)  # ADC Channel 3

# Setup OPCUA server
# reqires a hotspot hosted on the raspberry
IP = "10.0.0.10"
url = "opc.tcp://"+IP+":4840"
server = Server()
name = "OPCUA_Server"
addspace = server.register_namespace(name)
obj = server.nodes.objects.add_object(addspace, "Parameters")

OPCUA_Temp = obj.add_variable(f"ns={addspace};s=temperature", "Temperature", 0)
OPCUA_Valve = obj.add_variable(f"ns={addspace};s=valve", "Valve", 0)
OPCUA_Flow = obj.add_variable(f"ns={addspace};s=flow", "Flow", 0)
OPCUA_Pres = obj.add_variable(f"ns={addspace};s=pressure", "Pressure", 0)

# OLED Display
show_display = 0
MAX_DISPLAY_NUMBER = 5

# FUNCTIONS -----

def server_start():
    global server, url
    server.set_endpoint(url)
    server.start()
    oled_clear()
    oled_text("Server started")
    #print("Server started")
    oled.image(image)
    oled.show()
    sleep(0.5)

def reMap(value, maxInput, minInput, maxOutput, minOutput):

	value = maxInput if value > maxInput else value
	value = minInput if value < minInput else value

	inputSpan = maxInput - minInput
	outputSpan = maxOutput - minOutput

	scaledThrust = float(value - minInput) / float(inputSpan)

	return minOutput + (scaledThrust * outputSpan)


def oled_update():
    global show_display, oled, Temp, Pres, Valve, Flow
    oled_clear()
    if show_display == 0:     # Pi Stats Display
        now_time = datetime.now()
        date_str = now_time.strftime("%Y-%m-%d")
        time_str = now_time.strftime("%H:%M:%S")
        oled_text("IP: " + str(IP))
        oled_text("Last update: ", 1)
        oled_text("Date: " + str(date_str), 2)
        oled_text("Time: " + str(time_str), 3)

    elif show_display == 1:
        oled_text("Temp:",)
        oled_text(str(Temp), 1, font=font_large)
    elif show_display == 2:
        oled_text("Valve:")
        oled_text(str(Valve), 1, font=font_large)
    elif show_display == 3:
        oled_text("Pressure:")
        oled_text(str(Pres), 1, font=font_large)
    elif show_display == 4:
        oled_text("Flow:")
        oled_text(str(Flow), 1, font=font_large)
    elif show_display == 5:
        oled_text(f"Temp: {str(Temp)}", 0)
        oled_text(f"Valve: {str(Valve)}", 1)
        oled_text(f"Pres: {str(Pres)}", 2)
        oled_text(f"Flow: {str(Flow)}", 3)
    # Display image
    oled.image(image)
    oled.show()


def next_display():  # TODO instead of using threading, use logic instead og sleep
    global show_display
    show_display += 1
    if show_display > MAX_DISPLAY_NUMBER:
        show_display = 0
    oled_update()


def get_datavalue(value, variant_type=ua.VariantType.Int32):
    timestamp = datetime.now()
    variant = ua.Variant(value, variant_type)
    return ua.DataValue(variant, sourceTimestamp=timestamp)


def update_data(LOOPTIME: int):
    global Temp, Pres, Valve, Flow
    # Calls the function to get values from ADC
    Temp = round(ADC_Temp.value*100,1)
    Pres = int(reMap(ADC_Pres.value,0.20,0.04,1000,0))
    Valve = int(ADC_Valve.value*100)
    Flow = round(reMap(ADC_Flow.value,0.20,0,500,0),1)

    #print(f'{Temp } Grader celsius, {Pres } mBar, {Valve } % Open,{Flow } L/H')
    OPCUA_Temp.set_value(get_datavalue(float(Temp), ua.VariantType.Double))
    OPCUA_Pres.set_value(get_datavalue(Pres))
    OPCUA_Flow.set_value(get_datavalue(float(Flow), ua.VariantType.Double))
    OPCUA_Valve.set_value(get_datavalue(Valve))
    oled_update()
    sleep(LOOPTIME)

def oled_clear():
    draw.rectangle((0, 0, oled.width, oled.height), outline=0, fill=0)


def oled_text(string: str, line: int = 0, font=font_small, fill: int = 255):
    draw.text((0, line*16), string, font=font, fill=fill)

# PROGRAM START -----


oled_clear()
oled_text("starting server")
#print("Starting server")
oled.image(image)
oled.show()


server_start()  # If there is and IP, start the server

# Check if the ADC is getting a value
oled_text("Checking sensor", 1)
oled.image(image)
oled.show()
sleep(0.5)

adc_check = MCP3008(0).value*100
#print(f'Current value: {check}')

if 100 >= adc_check >= 0:
    #print("Value OK!")
    oled_text("Value OK!", 2)
    oled.image(image)
    oled.show()
else:
    #print("Input value not in acceptable range")
    exit()

try:
    update_data(0)
    button.when_pressed = next_display
    while True:
        update_data(1)

except KeyboardInterrupt:
    oled_clear()
    exit()
except Exception as e:
        print(e)
        oled_clear()
        exit()
finally:
    loop = False
    #print("goodbye")
    server.stop()
    oled_clear()
    exit()

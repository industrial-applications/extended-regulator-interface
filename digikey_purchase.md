# Digikey purchase list

| Compenent                 |Model      |Qauntity|Price[DKK]|Purchase link                                                                                                                                       |Datasheet |
| ------------              | ------------  |--| ------- | ------------ | ------------ |
|Digital isolator           |ISO7310CDR     |4 |21    |https://www.digikey.dk/da/products/detail/texas-instruments/ISO7310CDR/4946350?s=N4IgTCBcDaIJIGUDyB2AzARgAwGEAiASiALoC%2BQA                        |https://www.ti.com/general/docs/suppproductinfo.tsp?distId=10&gotoUrl=https%3A%2F%2Fwww.ti.com%2Flit%2Fgpn%2Fiso7310c
|Female pin headers 2x20    |PPTC202LFBN-RC |1 |17.15 |https://www.digikey.dk/da/products/detail/sullins-connector-solutions/PPTC202LFBN-RC/807240?s=N4IgTCBcDaIAQAUEBUDCYAMYAyAxAQgHIC0ASqiALoC%2BQA     |
|Female pin header 1x4      |PPPC041LFBN-RC |1 |3.5   |https://www.digikey.dk/da/products/detail/sullins-connector-solutions/PPPC041LFBN-RC/810176                                                        |
|Zener Diode                |BZT52C5V6-13   |5 |1.65  |https://www.digikey.dk/da/products/detail/diodes-incorporated/BZT52C5V6-13-F/1534399                                                               |
|Resistor                   |1206 1k        |20|0.63  |https://www.digikey.dk/da/products/detail/stackpole-electronics-inc/RNCP1206FTD1K00/2240337?s=N4IgTCBcDaIE4DsDGAHAjGADANgGYBcATNAa00yXwFoFCQBdAXyA
|DC - DC isolated converter |177920511      |1 |42.11 |https://www.digikey.dk/da/products/detail/w%C3%BCrth-elektronik/177920511/6621997                                                                  |https://www.we-online.com/katalog/datasheet/1779205311.pdf|
|Switch                     |EVQ-PUA02K     |2 |4.71  |https://www.digikey.dk/da/products/detail/panasonic-electronic-components/EVQ-PUA02K/286334?s=N4IgTCBcDaIKIDUCKBaACgVQIIAYwGkQBdAXyA               |
|Stand off                  |971110155      |4 |3.4   |https://www.digikey.dk/da/products/detail/w%C3%BCrth-elektronik/970110151/6174769                                                                  |https://www.we-online.com/katalog/datasheet/970xxxxx1_overview.pdf|
|Voltage reference          |ADR5045        |2 |12.66 |https://www.digikey.dk/da/products/detail/analog-devices-inc/ADR5045BRTZ-REEL7/1534669                                                             |https://www.analog.com/media/en/technical-documentation/data-sheets/ADR5040_5041_5043_5044_5045.pdf
|Skrue M3 10m               |97791003111    |8 |2.08  |https://www.digikey.dk/da/products/detail/w%C3%BCrth-elektronik/97791003111/10056389                                                               |https://www.we-online.com/katalog/datasheet/97791003111.pdf|
|**TOTAL without pi and components in ELAB**|| |232.93|


# 2022 09 12
ADC test  
research on isolation the adc from a RasPi (Digital isolators and DC to DC isolation)

# 2022 09 19

Recieved components from WE optocoupler and DC to DC isolater.  

Zener diode and voltage circuit test complete and works like intended  
Tested different zener dioder. 5V1, 5V6 and 6V2. 6V2 was most optimal.  

MCP3008 (ADC) tested and working. 
Optocoupler from WE are too slow (18µs) the MCP3008 requires speeds in the low ns.

DC to DC isolater tested with a capacitor on output and input side and functions like expected. Blocks voltage on output side.

# 2022 09 26
BoM  

OPC client and server scripts  
Auto-starts server and assigns current IP address  
Added code from raspberry. NOTE: Working OLED display and sensor  

Circuit Schematic made
# 2022 10 03
Use case  

BoM updated with all components required for PCB

PCB made and ordered  

UPC UA server  
Communication with Nodered

NodeRed dashboard working with OPC UA
# 2022 10 10
Case for Raspberry Pi, was worked on, and first test 3D print.

Python script for OPC UA:
- How handles connect and disconnect from network.
- Button for changing menu implemented.

# 2022 10 12
PCB assembly and test.

Test resulted in:
- Digital isolation and spi comunication working.
- DC DC working, but with output voltage of ~5.45V.
- ADC:
    - Zener diode starts to breakdown at ~3.5V.
    - High reference voltage (~5.45V from DC DC)
    - Results in inaccurate readings. (example: showing 28% flow when it should be 50%)

# 2022 10 13
Turns out Zener diodes does not operate properly at low current.

Voltage divider and current limiting diodes on input changed from 100k to 1k. This allows more current (~5mA) to flow for the Zener diodes.

Test:
- Test input with voltage divider shows around 10% diviation. Hvis deviation is caused by the 5.45V (~10% too high) from the DC DC isolator.
